#ifndef UTIL_HPP
#define UTIL_HPP

#include "llvm/Pass.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/Support/raw_ostream.h"
#include <string>
#include <iostream>

#include <cxxabi.h>

std::string demangle(const char* name);

void printType(llvm::Type *t);


#endif
