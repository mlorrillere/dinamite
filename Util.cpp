#include "Util.hpp"

std::string demangle(const char* name) 
{
    int status = -1; 

    std::unique_ptr<char, void(*)(void*)> res { abi::__cxa_demangle(name, NULL, NULL, &status), std::free };
    return (status == 0) ? res.get() : std::string(name);
}

void printType(llvm::Type *t) {
    std::string type_str;
    llvm::raw_string_ostream rso(type_str);
    t->print(rso);
    std::cerr << "Type: " << rso.str() << std::endl;
}
