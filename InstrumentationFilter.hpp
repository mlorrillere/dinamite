#ifndef INSTRUMENTATION_FILTER_HPP
#define INSTRUMENTATION_FILTER_HPP

#include "json11.hpp"

#include <iostream>
#include <string>
#include <fstream>
#include <set>

using namespace std;
using namespace json11;

class InstrumentationFilter {
    private:
        Json filters;
        bool loaded;
        ofstream outfile;
        set<string> functions;
    public:
        InstrumentationFilter() : loaded(false), outfile("functions.out", ios_base::app) {
		cerr << "DIN_FILTERS: CONSTRUCTOR" << endl;		
	};
        ~InstrumentationFilter() {
            for (auto f : functions) {
                outfile << f << endl;
            }
            outfile.close();
        }
        void loadFilterData(const char *filename);
        void loadFilterDataEnv();
        bool checkFunctionFilter(string function_name, string event_type);
        bool checkFunctionArgFilter(string function_name, int arg);
};

#endif
