#ifndef IDMAP_HPP
#define IDMAP_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

using namespace std;

struct IdMap {
    map<string, int> id_map;
    int maxSrc = 0;
    string fname;

    IdMap(string filename) {
        const char * val = ::getenv("DIN_MAPS");
        string prefix = "./";
        if ((val == 0) || (strcmp(val,"") == 0)) {
            cerr << "DIN_MAPS not set, falling back to current directory" << endl;
        } else {
            prefix = val;
            prefix += "/";
        }

        ifstream mapin;
        fname = prefix + filename;
        mapin.open(fname);
        if (!mapin.is_open()) {
            return;
        }
        stringstream ss;
        string err;
        ss << mapin.rdbuf();
        json11::Json jsmap;
        jsmap = json11::Json::parse(ss.str(), err);

        for (auto entry : jsmap.object_items()) {
            id_map[entry.first] = entry.second.int_value();
        }
        maxSrc = id_map.size();
    }


    void saveMap() {
        ofstream mapout;
        mapout.open(fname, ofstream::trunc);
        json11::Json jsmap(id_map);
        mapout << jsmap.dump();
        mapout.flush();
        mapout.close();
    }

    int getId(string src) {
        if (id_map.count(src) == 0) {
            id_map[src] = maxSrc++;
            return maxSrc - 1;
        } else {
            return id_map[src];
        }
    }

};


#endif
