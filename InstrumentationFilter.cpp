#include "InstrumentationFilter.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

using namespace json11;
using namespace std;

void InstrumentationFilter::loadFilterDataEnv() {
    const char * val = ::getenv("DIN_FILTERS");
    if ((val == 0) || (strcmp(val,"") == 0)) {
        cerr << "DIN_FILTERS not set, allowing all instrumentation" << endl;
        return;
    }

    loadFilterData(val);
    cerr << "DIN_FILTERS created" << endl;
}

void InstrumentationFilter::loadFilterData(const char *filename) {

	cerr << "DIN_FILTERS with " << filename << " file " << endl;
	ifstream fin;
	fin.open(filename);
	if (!fin.is_open()) {
        cerr << "Error: Couldn't open " << filename << ", filters not loaded." << endl;
		return;
	}
	stringstream ss;
	ss << fin.rdbuf();
	string err;

	this->filters = Json::parse(ss.str(), err);

	loaded = true;
	cerr << "FILTER: Loaded set to true!" << endl;
}


bool InstrumentationFilter::checkFunctionFilter(string function_name, string event_type) {

	if (!loaded) {
		//cerr << "FILTER: Loaded IS NOT SET!" << endl;
		return true; // No filters defined
	}

    if (function_name.compare("*") != 0) {
        functions.insert(function_name); // Add to the list of encountered functions, mostly used for mangled stuff
        if (checkFunctionFilter("*", event_type)) {
            return true;
        }
    }

	Json eventarray = filters["function_filters"][function_name]["events"];
	for (auto it : eventarray.array_items()) {
		if (event_type.compare(it.string_value()) == 0) {
			return true;
		}
	}

	return false;
}

bool InstrumentationFilter::checkFunctionArgFilter(string function_name, int arg) {
    if (!loaded) return false; // don't print args if we haven't enabled it explicitly

	Json argfilter = filters["function_filters"][function_name]["arguments"];
    if (argfilter.is_array()) {
        for (auto it : argfilter.array_items()) {
            if (it.int_value() == arg) {
                return true;
            }
        }
    } else if (argfilter.is_string()) {
        if (argfilter.string_value().compare("*") == 0) {
            return true;
        }

    }

    return false;
}
